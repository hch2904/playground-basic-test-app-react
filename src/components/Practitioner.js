import React, { useEffect, useState } from "react";
import { getPractitioners } from "../services";

import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import PractitionerCard from "./practitionerCard";
import { Container, LinearProgress, Typography } from "@material-ui/core";
import { useSnackbar } from "notistack";

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    overflow: 'hidden',
  },
  gridList: {
    padding: `${theme.spacing(4)} 0`
  }
}));

const Practitioner = (props) => {
  const classes = useStyles();
  const [practitioners, setPractitioners] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const { enqueueSnackbar } = useSnackbar();

  const flattenPractitionerObj = (response) => {
    return (response.data.entry || []).map((item) => {
      const name = item.resource.name || [];
      return {
        id: item.resource.id,
        name: `${((name[0] || {}).given || []).join(" ")} ${(name[0] || {}).family || "N/A"}`,
        gender: item.resource.gender,
        dob: item.resource.birthDate,
        photo: "https://cdn.pixabay.com/photo/2016/08/08/09/17/avatar-1577909_1280.png",
      };
    });
  };

  useEffect(() => {
    const go = async () => {
      try {
        setIsLoading(true);
        const response = await getPractitioners();
        setPractitioners(flattenPractitionerObj(response));
      } catch (error) {
        console.log(error);
        enqueueSnackbar("There was an error while fetching the practitioners", { variant: "error" })
      } finally {
        setIsLoading(false);
      }
    };
    go();
  }, [enqueueSnackbar]);

  const handleDelete = (id) => {
    const newItems = practitioners.filter((practitioner) => practitioner.id !== id);
    setPractitioners(newItems);
  }

  return (
    <>
      <Container>
        <Typography variant="h4" color="textSecondary">Practitioners</Typography>
        {isLoading && <LinearProgress color="secondary" />}
        <Grid container spacing={2} justify="flex-start" className={classes.gridList}>
          {practitioners.map((practitioner) => <Grid item md={4} key={practitioner.id}>
            <PractitionerCard
              id={practitioner.id}
              name={practitioner.name}
              birthdate={practitioner.dob}
              gender={practitioner.gender}
              handleDelete={handleDelete}
            />
          </Grid>)}
        </Grid>
      </Container>
    </>
  );
}

export default Practitioner;
