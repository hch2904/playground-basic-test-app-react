import { Box, Typography } from '@material-ui/core';
import React, { Component } from 'react';

class ErrorBoundary extends Component {

  constructor (props) {
    super(props);
    this.state = {
      error: null,
      errorInfo: null
    };
  }

  componentDidCatch (error, errorInfo) {
    this.setState({
      error,
      errorInfo
    });
  }

  render () {
    const { error, errorInfo } = this.state;
    if (errorInfo) {
      // eslint-disable-next-line no-undef
      const errorDetails = process.env.NODE_ENV === 'development' ? 
        ( 
          <details className="preserve-space">
            {error && error.toString()}
            <br />
            {errorInfo.componentStack}
          </details>
        ) : (
          null
        );
      return (
        <Box m={2}>
          <Typography color="error" variant="h5">An unexpected error has occurred.</Typography>
          {errorDetails}
        </Box>
      );
    }
    return this.props.children;
  }
}

export default ErrorBoundary;
