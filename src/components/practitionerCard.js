import React from 'react';
import {
  Card,
  CardActions,
  CardContent,
  Avatar,
  Typography,
  Divider,
  IconButton,
  Tooltip
} from '@material-ui/core';

import dayjs from "dayjs";

import DeleteIcon from '@material-ui/icons/Delete';

import { makeStyles } from '@material-ui/styles';


const useStyles = makeStyles(theme => ({
  root: {},
  details: {
    display: 'flex',
    justifyContent: 'center'
  },
  avatar: {
    margin: '0 auto',
    flexShrink: 0,
    flexGrow: 0,
    marginBottom: theme.spacing(1),
    width: theme.spacing(6),
    height: theme.spacing(6),
    backgroundColor: theme.palette.secondary.dark,
  },
  textDetails: {
    textAlign: 'center'
  },
  cardAction: {
    textAlign: "center",
    justifyContent: "center"
  }
}));

const PractitionerCard = (props) => {
  const classes = useStyles();
  const { id, name, gender, birthDate, handleDelete, ...rest } = props;
  return (
    <Card
      {...rest}
      className={classes.root}
    >
      <CardContent className={classes.content}>
        <Avatar
          className={classes.avatar}
          variant="circular"
        >
          {name && name[0]}
        </Avatar>
        <div className={classes.details}>
          <div className={classes.textDetails}>
            <Typography variant="h5">
              {name ? name : "N/A"}
            </Typography>
            <Typography variant="subtitle1" display="block" >
              Date of birth: {birthDate ? dayjs(birthDate).format('YYYY/MM/DD') : "N/A"}
            </Typography>
            <Typography
              gutterBottom
              variant="caption"
              color="textSecondary"
            >
              Gender {gender ? gender : "N/A"}
            </Typography>
          </div>
        </div>
      </CardContent>
      <Divider />
      <CardActions className={classes.cardAction}>
        <Tooltip title="Delete Practitioner">
          <IconButton onClick={() => handleDelete(id)} size="small" color="primary" className={classes.icon} >
            <DeleteIcon fontSize="small" />
          </IconButton>
        </Tooltip>
      </CardActions>
    </Card>
  );
}

export default PractitionerCard;