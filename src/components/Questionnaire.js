import React from "react";
import crypto from "crypto";

import questions from '../assets/questionnaire.json';

import { Formik, Form } from 'formik';
import * as Yup from "yup";
import { Box, Button, Checkbox, FormControlLabel, Grid, Paper, TextField, Typography } from "@material-ui/core";
import { useSnackbar } from "notistack";

const QUESTION_TYPES = {
  DATE: "date",
  STRING: "string",
  BOOLEAN: "boolean",
  GROUP: "group"
}

const getSHA = (item) => {
  const shaSum = crypto.createHash('sha1')
  shaSum.update(item)
  return shaSum.digest('hex');
}

const Question = (props) => {
  const { type, linkId, values, text, errors, touched, handleChange } = props;
  let question = null;
  const fieldName = getSHA(linkId);
  switch (type) {
    case QUESTION_TYPES.BOOLEAN:
      question = (
        <FormControlLabel
          control={
            <Checkbox
              id={fieldName}
              type="checkbox"
              label={text}
              name={fieldName}
              checked={values[fieldName]}
              onChange={handleChange}
            />
          }
          label={text}
        />)
      break;
    case QUESTION_TYPES.STRING:
      question = (<TextField
        id={fieldName}
        type="text"
        label={text}
        name={fieldName}
        value={values[fieldName]}
        error={Boolean(touched[fieldName] && errors[fieldName])}
        helperText={touched[fieldName] && "Required"}
        onChange={handleChange}
        fullWidth
      />)
      break;
    case QUESTION_TYPES.DATE:
      question = (<TextField
        id={fieldName}
        type="date"
        label={text}
        name={fieldName}
        value={values[fieldName]}
        onChange={handleChange}
        error={Boolean(touched[fieldName] && errors[fieldName])}
        helperText={touched[fieldName] && "Required"}
        InputLabelProps={{ shrink: true }}
        fullWidth
      />)
      break;
    case QUESTION_TYPES.GROUP:
      question = (<>
        <Typography variant="h5" color="textPrimary" gutterBottom>{props.text}</Typography>
        {
          props.item.map((item) => {
            return <Box my={2} key={getSHA(item.linkId)}>
              <Question
                key={getSHA(item.linkId)}
                handleChange={handleChange}
                values={values}
                errors={errors}
                touched={touched}
                {...item}
              />
            </Box>
          })
        }
      </>)
      break;
    default:
      break;
  }

  return question
};

const Questionnaire = () => {

  const {enqueueSnackbar} = useSnackbar();

  const getInitValues = (questions) => {
    return questions.reduce((acc, curr) => {
      if (curr.type === QUESTION_TYPES.GROUP) {
        return { ...getInitValues(curr.item), ...acc };
      }
      if (curr.type === QUESTION_TYPES.BOOLEAN) {
        const fieldName = getSHA(curr.linkId);
        acc[fieldName] = false;
        return acc;
      }
      const fieldName = getSHA(curr.linkId);
      acc[fieldName] = "";
      return acc;
    }, {});
  }

  const buildResponseValues = (questions, values) => {
    return questions.map((question) => {
      if (question.type === QUESTION_TYPES.GROUP) {
        return { ...question, item: buildResponseValues(question.item, values) }
      }
      return {
        linkId: question.linkId,
        text: question.text,
        answer: values[getSHA(question.linkId)]
      }
    });
  };

  const createSchema = (questions, initialSchema) => {
    questions.forEach((item) => {
      if (item.item) {
        createSchema(item.item, initialSchema);
      } else {
        let validation;
        if (item.type === QUESTION_TYPES.STRING || item.type === QUESTION_TYPES.DATE) {
          validation = Yup.string().required('Required');
        }
        initialSchema[getSHA(item.linkId)] = validation;
      }
    });
    return Yup.object().shape(initialSchema);
  };

  const submitForm = (values, formik) => {
    const response = {
      Identifier: {
        value: questions.id,
      },
      status: questions.status,
      subject: questions.subjectType,
      item: buildResponseValues(questions.item, values),
    }
    enqueueSnackbar("Form submitted successfully. Please check the console for further info!", {variant: "success"});
    console.log(response);
    formik.resetForm();
  }

  const schema = createSchema(questions.item, {});
  const initValues = getInitValues(questions.item);


  return (
    <div>
      <Box p={2}>
        <Paper variant="elevation">
          <Box p={2}>
            <Grid container spacing={1} justify="center">
              <Grid item md={10} xs={12}>
                <Typography variant="h4" color="textSecondary">Questionnaire</Typography>
                <Formik
                  initialValues={initValues}
                  onSubmit={submitForm}
                  validationSchema={schema}
                >
                  {({ values, errors, touched, handleChange }) => (
                    <Form>
                      {questions.item.map((question) => {
                        return <Box my={2} key={getSHA(question.linkId)}>
                          <Question
                            key={getSHA(question.linkId)}
                            values={values}
                            handleChange={handleChange}
                            errors={errors}
                            touched={touched}
                            {...question}
                          />
                        </Box>
                      })}
                      <Button
                        type="submit"
                        color="primary"
                        variant="contained"
                        fullWidth
                      >
                        Submit
                      </Button>
                    </Form>
                  )}
                </Formik>
              </Grid>
            </Grid>
          </Box>
        </Paper>
      </Box>
    </div>
  )
};

export default Questionnaire;