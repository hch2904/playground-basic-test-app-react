import React, { Component } from "react";
import dayjs from "dayjs";

import { getPatients } from "../services";

import Box from '@material-ui/core/Box';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { withStyles } from '@material-ui/core/styles';
import { Button, Container, LinearProgress, TextField, Typography } from "@material-ui/core";

const styles = {
  root: {
    width: '100%',
  },
  container: {
    maxHeight: "calc(100vh - 280px)",
  },
  note: {
    marginBottom: 0
  }
};

class PatientTable extends Component {
  state = {
    patients: [],
    searchQuery: "",
    searchDob: "",
    validationError: false,
    isLoading: false,
    moment: dayjs()
  };

  columns = [
    { id: 'name', label: 'Name' },
    { id: 'gender', label: 'Gender' },
    { id: 'dob', label: 'Date of Birth' },
  ]

  fetchPatients = () => {
    const params = {};
    const { searchDob, searchQuery } = this.state;
    if (searchQuery) {
      params.name = searchQuery
    }
    if (searchDob) {
      params.birthdate = `ge${searchDob}`
    }
    this.setState({ isLoading: true }, () => {
      getPatients(params)
        .then((res) => {
          this.setState({
            patients: this.buildPatientRows(res.data),
            moment: new Date()
          });
        })
        .catch(err => console.log(err))
        .finally(() => this.setState({ isLoading: false }));
    })
  };

  componentDidMount() {
    this.fetchPatients();
  }

  buildPatientRows = (data) => {
    return (data.entry || [])
      .sort((a, b) => {
        return new Date(b.resource.birthDate).getTime() - new Date(a.resource.birthDate).getTime();
      })
      .map((item) => {
        const { resource } = item;
        const name = resource.name || [];
        return {
          id: resource.id,
          name: `${((name[0] || {}).given || []).join(" ")} ${(name[0] || {}).family || "N/A"}`,
          gender: resource.gender || "N/A",
          dob: resource.birthDate || "N/A",
        };
      });
  };

  handleSearchChange = (event) => {
    this.setState({ searchQuery: event.target.value })
  };

  handleDateChange = (event) => {
    this.setState({ searchDob: event.target.value })
  };

  onSearch = (event) => {
    const regex = /^[a-zA-Z ]+$/g;
    const { searchQuery } = this.state;
    if (!regex.test(searchQuery)) {
      return this.setState({ validationError: true })
    }
    this.fetchPatients();
  }

  reset = () => {
    this.setState({ searchDob: "", searchQuery: "" }, () => {
      this.fetchPatients();
    });
  }

  render() {
    const { classes } = this.props;
    const { patients, searchDob, searchQuery, validationError, moment, isLoading } = this.state;
    return (
      <>
        <Container>
          <Box m={2}><Typography variant="h4" color="textSecondary">Patients</Typography></Box>
          <Box display="flex" justifyContent="space-between" alignContent="baseline" m={2}>
            <TextField
              type="text"
              label="Search by name"
              placeholder="John Doe"
              value={searchQuery}
              error={validationError}
              onChange={this.handleSearchChange}
              helperText={validationError && "Only text can be entered"}
            />
            <TextField
              type="date"
              label="Date of Birth"
              value={searchDob}
              InputLabelProps={{ shrink: true }}
              onChange={this.handleDateChange}
            />
            <Button
              onClick={this.onSearch}
              className="search-button"
              disabled={!searchDob && !searchQuery}
              variant="outlined"
            >
              Search
          </Button>
            <Button
              onClick={this.reset}
              disabled={!searchDob && !searchQuery}
              className="search-button"
              variant="outlined"
            >
              Reset
          </Button>
          </Box>
          <Box mx={2}>
            <Typography className={classes.note} color="textPrimary" variant="subtitle1">Results as of {dayjs(moment).format('YYYY-MM-DD')} at {dayjs(moment).format('HH:mm')}</Typography>
          </Box>
          {
            isLoading && <Box m={2}>
              <LinearProgress color="secondary" />
            </Box>
          }
          <Box p={2}>
            <Paper className={classes.root} elevation={3} variant="elevation">
              <TableContainer className={classes.container}>
                <Table stickyHeader aria-label="sticky table">
                  <TableHead>
                    <TableRow>
                      {this.columns.map((column) => (
                        <TableCell
                          key={column.id}
                        >
                          {column.label}
                        </TableCell>
                      ))}
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {patients.map((row) => {
                      return (
                        <TableRow hover role="checkbox" tabIndex={-1} key={row.id}>
                          {this.columns.map((column) => {
                            return (
                              <TableCell key={column.id} align={column.align}>
                                {row[column.id]}
                              </TableCell>
                            );
                          })}
                        </TableRow>
                      );
                    })}
                  </TableBody>
                </Table>
              </TableContainer>
            </Paper>
          </Box>
        </Container>
      </>
    );
  }
}

export default withStyles(styles)(PatientTable);
