import React from "react";
import {
  HashRouter as Router,
  Switch,
  Route,
  Link,
} from "react-router-dom";
import { Box, Button } from "@material-ui/core";

import PatientTable from "./components/Patient";
import Practitioner from "./components/Practitioner";
import Questionnaire from "./components/Questionnaire";

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  appBar: {
    display: 'flex',
    background: theme.palette.primary.light
  },
  gridList: {
    padding: `${theme.spacing(4)} 0`
  },
  link: {
    textDecoration: "none",
    color: "white",
  }
}));

export default function Routes() {
  const classes = useStyles();
  return (
    <Router>
      <Box py={2} mb={2} display="flex" className={classes.appBar} >
        <Link to="/" className={classes.link}>
          <Button className={classes.link}>
            Patient
          </Button>
        </Link>
        <Link to="/practitioner" className={classes.link}>
          <Button className={classes.link}>
            Practitioner
          </Button>
        </Link>
        <Link to="/question" className={classes.link}>
          <Button className={classes.link}>
            Questionnaire
          </Button>
        </Link>
      </Box>
      <Switch>
        <Route path="/practitioner" component={() => <Practitioner />} />
        <Route path="/question" component={() => <Questionnaire />} />
        <Route path="/" component={() => <PatientTable />} />
      </Switch>
    </Router>
  );
}