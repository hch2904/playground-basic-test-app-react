import React, { Component } from "react";
import Routes from "./routes";

import { ThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import { SnackbarProvider } from "notistack";
import ErrorBoundary from "./components/ErrorBoundry";

const appTheme = createMuiTheme({
  palette: {
    primary: {
      'main': '#0c254a',
      'light': '#3c4d76',
      'dark': '#000023',
      'contrastText': '#FFF',
    },
    secondary: {
      'main': '#20e8db',
      'light': '#71ffff',
      'dark': '#00b5a9',
      'contrastText': '#000',
    },
    error: {
      'main': '#ff2d55',
      'light': '#ff6b81',
      'dark': '#c4002d',
      'contrastText': '#000',
    },
    text: {
      primary: "#0c254a",
      secondary: "#4d567f"
    }
  },
  typography: {
    // Use the system font instead of the default Roboto font.
    fontFamily: [
      '-apple-system',
      'Roboto',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(',')
  }
});

class App extends Component {
  render() {
    return (
      <ErrorBoundary>
        <ThemeProvider theme={appTheme}>
          <SnackbarProvider
            anchorOrigin={{ horizontal: "right", vertical: "top" }}
          >
            <Routes />
          </SnackbarProvider>
        </ThemeProvider>
      </ErrorBoundary>
    )
  }
}

export default App;
